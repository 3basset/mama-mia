#include <iostream>
#include "functions/add.h"
#include "functions/power.h"
#include "functions/subtract.h"
#include "functions/divide.h"
#include "functions/sqrt.h"
#include "functions/todeci.h"

using namespace std;

int main()
{
  int x;
  cout << endl <<"A program made by the CS student under the supervision of DR. Khafagy. You pick the operations and the systems you want to pick by entering the number beside them." <<endl<<endl<<endl<<endl;

  cout << "Please enter what you want to do:-" << endl << "1- Normal Calculator (Plus, Minus, divide, square root, ect.). \n" << "2- Switching from number bases.\n" << "3- ASCII code finder (From ASCII to Characters)\n" << "4- ASCII code finder (From Characters to ASCII)\n" << "System: ";
  cin >> x;
  cout <<endl;
    if (x == 1)
      {
        int z;
        int l=1;
        double x, y;
        cout << "What is the type of operation that you'll do?:-\n" << "1- Addition\n" << "2- Subtraction\n" << "3- Division\n" << "4- Multiplication\n" << "5- Square root\n" << "6- Power\n" "Operation: " ;
        cin >> z;
        cout <<endl;
            while (l!=0){
          if (z == 1){
            add();
            cout << "Enter 0 to exit or 1 to perform another operation.";
            cin >> l;

          }
          else if (z == 2){
            subtract();
            cout << "Enter 0 to exit or 1 to perform another operation.";
            cin >> l;


          }
          else if (z == 3){
            divide();
            cout << "Enter 0 to exit or 1 to perform another operation.";
            cin >> l;
          }
          else if (z == 4){
            cout << "Enter the first number: ";
            cin >> x;
            cout << "Enter the second number: ";
            cin >> y;
            cout << "The sum of the multiplication is: " << x*y<<endl;
            cout << "Enter 0 to exit or 1 to perform another operation.";
            cin >> l;
          }
          else if (z == 5){
            squarert();
          }
          else if (z == 6){

            cout << "Enter 0 to exit or 1 to perform another operation.";
            cin >> l;
          }
          else
            cout << "This is not a valid operation!";
      }}
    else if (x == 2)
      {
        todeci();


      }
    else if (x == 3)
      {
        int x;
        cout << "Enter the ASCII code: ";
        cin >> x;
        cout << "The ASCII code translation of that number is the character: " << char(x);

      }
    else if (x == 4)
      {
        char x;
        cout << "Enter the character you want to translate to ASCII: ";
        cin >> x;
        cout << "The ASCII code of that character is: " << int(x);

      }
    else
      cout << "This is not a valid choice!";


    return 0;
}
